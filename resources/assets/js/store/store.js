import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

function hasCity(city, inCities) {
    let match = false;
    for (let i = 0; i < inCities.length; i++) {
        if (inCities[i] == city) {
            match = inCities[i];//index == 0 == false
        }
    }
    return match;
};

let checkLocalStorageFor = function ($item) {
    let item;
    if (localStorage) {
        item = localStorage.getItem($item);
    }
    if (item == null || !item) {
        return false
    } else {
        return item;
    }
};


export const store = new Vuex.Store({
    state: {
        weather: {
            api_key_bkp: 'cbcd599f361056e888a65fd893ea4cac',
            api_key: '6fbc78b4c657a827a367aec672535700',
            search_methods: ['name', 'id', 'coordinates'],
            current_city: '',
            cities: [],
            current_weather: undefined,
            weekly_weather: undefined,
        }

    },
    getters: {
        weather_api_key(state)
        {
            return state.weather.api_key;
        },
        weather_search_methods(state){
            return state.weather.search_methods;
        },
        current_weather(state){
            return state.weather.current_weather;
        },
        weekly_weather(state){
            return state.weather.weekly_weather;
        },
        cities(state){
            return state.weather.cities;
        },
        current_city(state){
            return state.weather.current_city;
        }

    }
    ,
    mutations: {
        weather_api_key(state, $key)
        {
            state.weather.api_key = $key;
        },
        weather_search_methods(state, $methods){
            state.weather.search_methods = $methods;
        },
        current_weather(state, $weather){
            state.weather.current_weather = $weather;
        },
        weekly_weather(state, $weather){
            state.weather.weekly_weather = $weather;
        },
        cities(state, $cities) {
            state.weather.cities = $cities;

        },
        add_city(state, $city) {
            if (!hasCity($city, state.weather.cities)) {
                state.weather.cities.push($city);

                if (localStorage) {
                    localStorage.setItem('cities', JSON.stringify(state.weather.cities));
                }
            }

        },
        current_city(state, $city) {
            state.weather.current_city = $city;
            if (localStorage) {
                localStorage.setItem('current_city', state.weather.current_city);
            }
        },
    }
    ,
    actions: {
        changeWeatherApiKey({commit}, $key)
        {
            commit('weather_api_key', $key);
        },
        changeCurrentWeather({commit}, $weather)
        {
            commit('current_weather', $weather);
        },
        changeWeeklyWeather({commit}, $weather)
        {
            commit('weekly_weather', $weather);
        },
        addCity({commit}, $city) {
            if ($city && $city != null) {
                commit('add_city', $city);
            }
        },
        currentCity({commit}, $city) {
            if ($city && $city != null) {
                commit('current_city', $city);

            }
        },

    }

});
new Vue({
    store,
    data(){
        return {}
    },
    created(){
        this.checkLocalStorage();
    },
    methods: {
        checkLocalStorage(){
            let cities = checkLocalStorageFor('cities');
            let current_city = checkLocalStorageFor('current_city');

            if (cities) {
                this.$store.commit('cities', JSON.parse(cities))
            }
            if (current_city) {
                this.$store.dispatch('currentCity', current_city);
            }
        }
    }

});