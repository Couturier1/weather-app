let submitUrl;

export class Errors {
    constructor() {
        this.errors = {}
    }

    get(field) {
        if (this.errors[field]) {
            return this.errors[field];
        }
    }

    record(errors) {
        this.errors = errors;
    }

    clear(field) {
        let errorsState = this.errors;

        if (this.has(field)) {
            delete this.errors[field];
            errorsState = this.errors;

        } else {
            this.errors = {};
            this.errors = errorsState;
        }


    }

    clearAll() {
        this.errors = {}
    }

    addField(field, message) {

        if (!this.has(field)) {
            this.errors[field] = message;

        }

    }

    addError(field, message) {

        if (!this.has(field)) {
            this.errors[field] = message;

        }

    }

    has(field) {
        return this.errors.hasOwnProperty(field);

    }

    any() {
        return Object.keys(this.errors).length > 0;
    }

}

export class Form {
    constructor(data) {
        this.originalData = data;
        this.loading = false;

        for (let field in data) {
            this[field] = data[field];
            this[field.type] = data[field.type];
        }
        this.errors = new Errors();


    }

    data() {
        let data = Object.assign({}, this);

        delete data.originalData;
        delete data.errors;
        delete data.loading;

        return data;
    }


    reset() {
        for (let field in this.originalData) {
            this[field] = this.originalData[field];
        }

        this.errors.clearAll();
    }


}


//Vee-validator config
export const config = {
    errorBagName: 'v_errors', // change if property conflicts.
    fieldsBagName: 'fields',
    delay: 0,
    locale: 'en',
    dictionary: null,
    strict: true,
    enableAutoClasses: true,
    classNames: {
        touched: 'touched', // the control has been blurred
        untouched: 'untouched', // the control hasn't been blurred
        valid: 'valid', // model is valid
        invalid: 'invalid', // model is invalid
        pristine: 'pristine', // control has not been interacted with
        dirty: 'active' // control has been interacted with
    },
    events: 'input|change|blur',

    inject: true
};






